#
# Copyright 2022 Persival GmbH
# SPDX-License-Identifier: MPL-2.0
#

import json
import numpy as np
import matplotlib.pyplot as plt

# open JSON files
source_file_name = '/tmp/OSMPDummySource_flatbuf_timing_10-26-05.json'
model_file_name = '/tmp/OSMPDummySensor_flatbuf_timing_10-26-05.json'
source_file = open(source_file_name)
model_file = open(model_file_name)
source_data = json.load(source_file)
model_data = json.load(model_file)
source_file.close()
model_file.close()

# Source
source_events = source_data['Data'][0]['OsiEvents']
num_sim_time_steps = 0
start_source_serialize = 0
source_serialize_time = []
start_source = 0
source_time = []
for current_osi_event in source_events:
    if current_osi_event[0] == 1:
        start_source_serialize = current_osi_event[1]
        source_time.append(current_osi_event[1] - start_source)
    elif current_osi_event[0] == 2:
        source_serialize_time.append(current_osi_event[1] - start_source_serialize)
        num_sim_time_steps = num_sim_time_steps + 1
    elif current_osi_event[0] == 0:
        start_source=current_osi_event[1]
source_serialize_time = np.array(source_serialize_time)
source_mean_serialize_time = np.mean(source_serialize_time)
source_mean_gen_time = np.mean(np.array(source_time))
source_mean_time = source_mean_serialize_time + source_mean_gen_time

# Model
model_events = model_data['Data'][0]['OsiEvents']
start_serialize = 0
start_deserialize = 0
start_model = 0
model_serialize_time = []
model_deserialize_time = []
model_time = []
for current_osi_event in model_events:
    if current_osi_event[0] == 2:
        start_deserialize = current_osi_event[1]
    elif current_osi_event[0] == 3:
        model_deserialize_time.append(current_osi_event[1]-start_deserialize)
        start_model = current_osi_event[1]
    elif current_osi_event[0] == 0:
        start_serialize = current_osi_event[1]
        model_time.append(current_osi_event[1] - start_model)
    elif current_osi_event[0] == 1:
        model_serialize_time.append(current_osi_event[1]-start_serialize)

model_deserialize_time = np.array(model_deserialize_time)
model_serialize_time = np.array(model_serialize_time)
model_time = np.array(model_time)
model_mean_time = np.mean(model_deserialize_time) + np.mean(model_time) + np.mean(model_serialize_time)

# Plot
fig1, ax1 = plt.subplots()
serialization_lib = ''
if "flatbuf" in source_file_name:
    serialization_lib = 'Flatbuffers'
elif "protobuf" in source_file_name:
    serialization_lib = 'Protobuf'
ax1.set_title(serialization_lib + ' OSI in ' + str(num_sim_time_steps) + ' time steps')
ax1.boxplot([source_time, source_serialize_time, model_deserialize_time, model_time, model_serialize_time])
ax1.set_xticklabels(['Source gen.', 'Source ser.', 'Model deser.', 'Model calc.', 'Model ser.'])
props = dict(boxstyle='round', facecolor='white', alpha=0.5)
#textstr =  'Total serial. time: %.2fs' % (source_mean_serialize_time+np.mean(model_deserialize_time)+np.mean(model_serialize_time))
#ax1.text(0.65, 0.95, textstr, transform=ax1.transAxes, fontsize=10,
#        verticalalignment='top', bbox=props)
textstr = '\n'.join((
    r'Source time: %.2fs' % (source_mean_time, ),
    r'Model time: %.2fs' % (model_mean_time, )))
ax1.text(0.7, 0.95, textstr, transform=ax1.transAxes, fontsize=10,
        verticalalignment='top', bbox=props)
plt.ylabel('Time in s')
plt.show()
